![Aspire App](https://global-uploads.webflow.com/5ed5b60be1889f546024ada0/5ed8a32c8e1f40c8d24bc32b_Aspire%20Logo%402x.png)

# Aspire App POC

----------
to Run locally please follow below steps

```bash
$ yarn # from root path
```


- to run the Challenge code 1 run

```bash
$ yarn run:loan # runs both Font end  and Back end
```

- to run the Challenge code 2 run

```bash
$ yarn run:css 
```

to view online [Challenge code 2] [Demo](http://aspire-poc.surge.sh/)

----

> The application is using yarn workspace as mono repo and built on vue@latest for backend its running on express.js
>

### Working features

---

- [x] UI code development for Loan - Challenge - 1
- [x] CSS Challenge - Challenge - 2
- [x] Back-End
- [ ] TDD
