import 'glider-js/glider.css'
import './assets/style/style.scss'
import Glider from 'glider-js';

document.addEventListener('DOMContentLoaded', () => {
    document.querySelector("body").removeAttribute("hidden")
    new Glider(document.querySelector('.cards-wrap'),{
        slidesToShow: 1,
        dots: '#dots',
        draggable: true,
    });
})


