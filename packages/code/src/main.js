import {createApp} from 'vue'
import App from './App.vue'
import "./styles/app.scss";
import router from "./router";
import store from "./store";


const app = createApp(App)
app.use(router)
    .use(store)

app.mount('#app')