import {FETCH_USER, FETCH_USER_ERROR, FETCH_USER_SUCCESS} from "../../constants/auth";
import api from "../../../api";
import {
    APPROVE_PENDING_LOAN,
    CREATE_LOAN,
    CREATE_LOAN_ERROR,
    CREATE_LOAN_SUCCESS,
    FETCH_PENDING_LOAN,
    FETCH_PENDING_LOAN_SUCCESS
} from "../../constants/loan";

const state = {
    pendingLoans:[]
}

const getters = {
    pendingLoans:(state)=>{
        return state.pendingLoans
    }
}

const actions = {
    [CREATE_LOAN]: async ({dispatch, commit,store}, payload) => {
        const {amount,term,userId,email} = payload
        try {
            const res = await api.post("/loan/create", {
                amount,term,userId
            })
            if (res.data) {
                dispatch(FETCH_USER, {email})
            }
        } catch (e) {
            // commit(CREATE_LOAN_ERROR)
        }
    },
    [FETCH_PENDING_LOAN]:async ({commit})=>{
        try {
            const res = await api.get("/loan/yet-to-approve")
            if (res.data) {
                commit(FETCH_PENDING_LOAN_SUCCESS, res.data.loans)
            }
        } catch (e) {
        }
    },
    [APPROVE_PENDING_LOAN]:async ({commit,dispatch},payload)=>{
        try {
            const res = await api.put("/loan/update",{...payload})
            if (res.data) {
                dispatch(FETCH_PENDING_LOAN)

                // commit(FETCH_PENDING_LOAN_SUCCESS, res.data.loans)
            }
        } catch (e) {
            // commit(CREATE_LOAN_ERROR)
        }
    }
}

const mutations = {
    [FETCH_PENDING_LOAN_SUCCESS]: (state, payload) => {
        state.pendingLoans = payload
    },

}
export default {
    actions,
    mutations,
    getters
}