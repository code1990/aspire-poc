import {FETCH_USER, FETCH_USER_ERROR, FETCH_USER_SUCCESS, RESET_USER} from "../../constants/auth";
import api from "../../../api";
import router from "../../../router";

const getters = {
    loggedInUser: state => {
        return state.user
    },
    isAuthorized: state => {
        return state.authorized
    }
}

const state = {
    user: null,
    authorized: false
}
const actions = {
    [FETCH_USER]: async ({dispatch, commit}, payload) => {
        const {email} = payload
        try {
            const res = await api.get("/user/fetch", {params: {email}})
            if (res.data.status) {
                commit(FETCH_USER_SUCCESS, {...res.data.data})
            }
        } catch (e) {
            commit(FETCH_USER_ERROR)
        }
    }
}

const mutations = {
    [RESET_USER]: (state) => {
        state.authorized = false;
        state.user = null;
    },
    [FETCH_USER_SUCCESS]: (state, payload) => {
        state.user = payload;
        state.authorized = true;
    },
    [FETCH_USER_ERROR]: (state) => {
        state.user = null;
        state.authorized = false;
    }
}
export default {
    state,
    getters,
    actions,
    mutations
}