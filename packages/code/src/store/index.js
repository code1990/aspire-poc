import { createStore } from 'vuex'


import auth from './modules/auth';
import loan from './modules/loan';
const store = createStore({
    modules: {
        auth,
        loan
    },

})

export default store