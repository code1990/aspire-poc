import {createRouter, createWebHashHistory} from "vue-router";
import store from "../store";

const routes = [
    {
        path: "/",
        name: "Login",
        component: () => import ("../view/Login.vue"),
    },
    {
        path: "/user",
        name: "User",
        meta: {
            protected: true
        },
        component: () => import ("../view/User.vue"),
    },
    {
        path: "/admin",
        name: "Admin",
        meta: {
            protected: true,
            only: "admin"
        },
        component: () => import ("../view/Admin.vue"),
    }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes,
});

router.beforeEach((to, from, next) => {
    if (to.meta.protected && !store.state.auth.authorized) {
        next({name: 'Login'})
    }
    if (to.meta.protected && store.state.auth.authorized) {
        if (to.meta.only === "admin" && store.state.auth.user.type !== "admin") {
           return null
        } else {
            next()
        }
    }

    else {
        next()
    }
})

export default router;