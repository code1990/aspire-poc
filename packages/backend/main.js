import express from 'express';
import bodyParser from 'body-parser';
import moment from "moment";
import userRouter from "./routes/user";
import {dbConnect} from "./db";
import loanRouter from "./routes/loan";
import cors from 'cors';


dbConnect()
const appRouter = express.Router();
const version = "v1"
const app = express();
app.use(cors())
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json())
app.use(`/api/${version}`, appRouter)
appRouter.get("/ping", (req, res) => {
    res.send({
        date_time: (moment().unix() * 1000)
    })
})

appRouter.use("/user", userRouter);
appRouter.use("/loan", loanRouter);

app.listen(5000, () => {
    console.log("App listening on port ::: 5000")
})