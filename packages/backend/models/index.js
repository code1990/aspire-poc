import sequelize from "../db";
import {Sequelize} from 'sequelize';
import {DataTypes, Model} from 'sequelize';
import moment from "moment";


export class User extends Model {
}

User.init({
    firstName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    lastName: {
        type: DataTypes.STRING,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        index: true
    },
    type: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: "user"
    }
}, {
    sequelize,
    modelName: 'User'
})


export class Loan extends Model {
}

Loan.init({
    userId: {
        type: DataTypes.INTEGER,
        references: {
            model: User,
            key: 'id',
            deferrable: Sequelize.Deferrable.INITIALLY_IMMEDIATE,
        },
    },
    amount: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    term: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    isApprove: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    loanStartDate: {
        type: DataTypes.DATE
    }
}, {
    sequelize,
    modelName: 'Loan',
    hooks: {
        beforeUpdate(instance, options) {
            instance.loanStartDate = moment().utc().toDate()
        },
        afterUpdate(instance, options) {
            if (instance.isApprove) {
                const {amount, term} = instance;
                const payAmount = amount / term;
                const bulkArray = [];
                for (let i = 0; i < term; i++) {
                    bulkArray.push({
                        userId: instance.userId,
                        loanId: instance.id,
                        payAmount,
                        dueDate: moment(instance.loanStartDate).add(i + 1, "w").utc().toDate()
                    })
                }

                Instalments.bulkCreate(bulkArray);
            }


        }
    }
})


export class Instalments extends Model {
}

Instalments.init({
    dueDate: {
        type: DataTypes.DATE
    },
    payAmount: {
        type: DataTypes.DOUBLE
    },
    isPaid: {
        type: DataTypes.BOOLEAN,
        defaultValue: false
    },
    paidOn: {
        type: DataTypes.DATE
    }
}, {
    sequelize,
    modelName: 'Instalments',
    hooks: {
        beforeUpdate(instance, options) {
            instance.paidOn = moment().utc().toDate()
        }
    }
})

User.hasMany(Loan, {
    as: "loans"
})

Loan.belongsTo(User, {
    foreignKey: "userId",
})
Loan.hasMany(Instalments, {
    foreignKey: "loanId"
})

Instalments.belongsTo(User, {
    foreignKey: "userId"
})

/*
Instalments.belongsToMany(Loan,{
    through:"User",
    foreignKey:"userId",
    constraints:false
})
*/

/*
User.belongsToMany(Instalments, {
    foreignKey: "loanId",
    through:"Loan",
    constraints: false
})

Loan.belongsToMany(Instalments,{
    foreignKey: "userId",
    through:"User",
    constraints: false
})
*/

// User.hasMany(models.Award, {as: 'ifYouWantAlias', constraints: false, allowNull:true, defaultValue:null});

/*

User.belongsToMany(Instalments, {
    foreignKey: "userId",
    through: "Loan"
})*/
