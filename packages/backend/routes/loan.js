import {Router} from "express";
import {Loan, User} from "../models";

const loanRouter = Router()

loanRouter.post("/create", async (req, res) => {
    const loan = await Loan.create({
        ...req.body
    })
    await loan.save()
    res.send({
        msg: "create loan",
        loan
    })
})

loanRouter.put("/update", async (req, res) => {
    const loan = await Loan.findOne({
        where: {
            userId: req.body.userId,
            id: req.body.loanId
        }
    })

    if (loan) {
        const updatedLoan = await loan.update({
            isApprove: true
        })
        res.send({
            msg: "updated",
            updated: updatedLoan
        })
    }
})

loanRouter.get("/yet-to-approve", async (req, res) => {
    const loans = await User.findAll({
        include: [{
            model: Loan,
            where: {
                isApprove: false,
            },
            as: "loans"
        }]
    })
    res.send({
        loans
    })
})

export default loanRouter;