import {Router} from "express";
import {Instalments, Loan, User} from '../models/index'

const userRouter = Router()

userRouter.get("/fetch", async (req, res) => {
    if (!req.query.email) {
        res.status(401).send({
            msg: "email property missing"
        })
    }else{
        let foundUser = null
        try {
            foundUser = await User.findOne({
                where: {
                    email: req.query.email,
                },
                include: [{
                    model: Loan,
                    as: "loans",
                    include: [Instalments]
                }]
            })
        } catch (e) {
            console.log(e)
        }
        res.send({
            status: true,
            msg: "user exists",
            data: foundUser
        })
    }


})

userRouter.post("/create", async (req, res) => {
    let foundUser = null
    try {
        foundUser = await User.findOne({
            where: {
                email: req.body.email,
            },
            include: [{
                model: Loan,
                as: "loans",
                include: [Instalments]
            }]
        })
    } catch (e) {
        console.log(e)
    }

    if (foundUser) {
        res.send({
            msg: "user exists",
            data: foundUser
        })
    } else {
        const user = await User.create({
            ...req.body
        })
        await user.save()
        res.send({
            msg: "created user",
            data: user
        })
    }
})

export default userRouter;