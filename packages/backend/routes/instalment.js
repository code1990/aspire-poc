import {Router} from "express";
import {Instalments} from "../models";

const instalmentRouter = Router()

instalmentRouter.put("/update", async (req, res) => {
    const instalment = await Instalments.findOne({
        id: req.body.id
    });

    if (instalment) {
        await instalment.update({
            isPaid: true
        })

        res.send({
            msg:"updated",

        })
    }else{
        res.send({
            msg:"no installment by that id"
        })
    }

})

export default instalmentRouter;